# -----------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# -----------------------------------------------------------------------------------------------------------------

variable "hostname" {
  type        = string
  description = "Virtual machine hostname"
}

variable "domain" {
  type        = string
  description = "Domain name for generating fqdn"
}

variable "network_ip" {
  type        = string
  description = "IP Address (i.e. 192.168.122.10)"
}

variable "network_gw" {
  type        = string
  description = "Network gateway"
}

variable "nameservers" {
  type        = list(any)
  description = "DNS servers"
}

variable "dns_search_domains" {
  type        = list(any)
  description = "DNS search domains"
}

# -----------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters have reasonable defaults.
# -----------------------------------------------------------------------------------------------------------------

variable "cloud_image_path" {
  type        = string
  description = "Provide local path or URL to your cloud image template"
  default     = "/var/lib/libvirt/images/debian-11-generic-amd64.qcow2"
}

variable "disk_storage" {
  type        = string
  description = "Provide a storage pool where the resource will be created"
  default     = "default"
}

variable "disk_size" {
  type        = number
  description = "Amount of disk space in GB on the first virtual disk"
  default     = 20
}

variable "additional_disks" {
  type        = map(any)
  description = "Disk block used to configure additional disk devices"
  default     = {}
}

variable "cloud_image_user" {
  type        = string
  description = "Default user that should be created or enabled by cloud-init"
  default     = "root"
}

variable "cloud_image_user_password" {
  type        = string
  description = "Encrypted password for default cloud image user"
  default     = "$6$iItixmYBahH3RfLX$rSwEkkYjx9gnH9/xWwbx4oVNvNVoPTsYhy6g.pvB2/3Nc3lSyIdmgzdobIThh1Uxrvz1F/VY5wmLE96u0WMDq0"
}

variable "ssh_authorized_key" {
  type        = string
  description = "Provide SSH public key in plain text for initial deployment via cloud-init"
  default     = ""
}

variable "network_name" {
  type        = string
  description = "The name of an existing network to attach this interface to"
  default     = "default"
}

variable "network_mask" {
  type        = string
  description = "Network Mask"
  default     = "255.255.255.0"
}

variable "memory" {
  type        = string
  description = "Virtual machine memory in MB"
  default     = "2048"
}

variable "cpu_mode" {
  type        = string
  description = "Virtual machine CPU mode"
  default     = "host-passthrough"
}

variable "vcpu" {
  type        = number
  description = "Virtual machine vcpu"
  default     = 1
}

variable "qemu_agent" {
  type        = bool
  description = "Enable qemu-agent if installed in your template image"
  default     = true
}
