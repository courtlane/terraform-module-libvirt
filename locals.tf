locals {
  macaddress = macaddress.random.address
  fqdn       = "${var.hostname}.${var.domain}"
  system_disk_size = 1024 * 1024 * 1024 * var.disk_size


  template_user_data = templatefile("${path.module}/templates/user-data", {
    hostname                  = var.hostname
    cloud_image_user          = var.cloud_image_user
    cloud_image_user_password = var.cloud_image_user_password
    ssh_authorized_key        = var.ssh_authorized_key != "" ? var.ssh_authorized_key : file("~/.ssh/id_rsa.pub")
    fqdn                      = local.fqdn
  })

  template_network_config = templatefile("${path.module}/templates/network-config", {
    macaddress         = local.macaddress
    network_ip         = var.network_ip
    network_mask       = var.network_mask
    network_gw         = var.network_gw
    nameservers        = var.nameservers
    dns_search_domains = var.dns_search_domains
  })
}
