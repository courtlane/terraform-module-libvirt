resource "macaddress" "random" {
}

resource "libvirt_volume" "base_image" {
  name   = "${var.hostname}-base.qcow2"
  source = var.cloud_image_path
}

resource "libvirt_volume" "system_volume" {
  name           = "${var.hostname}-disk1.qcow2"
  base_volume_id = libvirt_volume.base_image.id
  pool           = var.disk_storage
  size           = local.system_disk_size
}

resource "libvirt_volume" "additional_disks" {
  for_each = var.additional_disks
  name           = "${var.hostname}-disk${index(keys(var.additional_disks), each.key) + 2}.qcow2"
  base_volume_id = null
  pool           = can(each.value.pool) ? each.value.pool : null
  size           = 1024 * 1024 * 1024 * each.value.size
}

resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.hostname}-cloudinit.qcow2"
  user_data      = local.template_user_data
  network_config = local.template_network_config
}

resource "libvirt_domain" "vm" {
  name       = var.hostname
  memory     = var.memory
  vcpu       = var.vcpu
  qemu_agent = var.qemu_agent

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  cpu {
    mode = var.cpu_mode
  }

  network_interface {
    network_name = var.network_name
    mac          = local.macaddress
  }

  disk {
    volume_id = libvirt_volume.system_volume.id
  }

  dynamic "disk" {
    for_each = var.additional_disks
    content {
      volume_id = "${libvirt_volume.additional_disks[disk.key].id}"
    }
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}
